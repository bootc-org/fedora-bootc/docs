* xref:getting-started.adoc[Getting Started]
* xref:base-images.adoc[Base images]
* xref:community.adoc[Discussion forums and community]
* Building containers
** xref:building-containers.adoc[Best Practices]
** xref:os-release-and-versions.adoc[Configuring the os-release file and version numbers]
** xref:building-custom-base.adoc[Building custom base images]
* Provisioning machines
** xref:bare-metal.adoc[Installing on Bare Metal]
** xref:podman-bootc-cli.adoc[Local virt testing with podman-bootc-cli]
** xref:qemu-and-libvirt.adoc[Using generic libvirt and qemu]
** xref:provisioning-aws.adoc[Installing on AWS]
** xref:provisioning-vsphere.adoc[Installing on vSphere]
** xref:provisioning-gcp.adoc[Installing on GCP (via OpenTofu)]
** xref:provisioning-generic.adoc[Installing on generic infrastructure]
** xref:cloud-agents.adoc[Understanding cloud agents]
** xref:provisioning-container.adoc[Running as a container for testing/inspection]
* System Configuration
** xref:authentication.adoc[Authentication, Users and Groups]
** xref:home-directories.adoc[Home directories]
** xref:dynamic-reconfiguration.adoc[Dynamic reconfiguration (e.g. Ansible)]
** xref:dnf.adoc[Using dnf]
** xref:storage.adoc[Configuring Storage]
** xref:sysconfig-network-configuration.adoc[Network Configuration]
** xref:running-containers.adoc[Running Containers]
** xref:container-pull-secrets.adoc[Container pull secrets]
** xref:security-and-hardening.adoc[Security and hardening]
** xref:embedding-containers.adoc[Embedding Containers]
** xref:hostname.adoc[Setting a Hostname]
** xref:proxy.adoc[Proxied Internet Access]
** xref:kernel-args.adoc[Modifying Kernel Arguments]
** xref:initramfs.adoc[The initial RAM disk (initrd)]
** xref:sysctl.adoc[Kernel Tuning]
** xref:sysconfig-setting-keymap.adoc[Setting Keyboard Layout]
** xref:counting.adoc[Node counting]
* System Architecture
** xref:filesystem.adoc[Filesystem layout]
** xref:generic-vs-build.adoc[Generic images and customization]
* OS updates
** xref:auto-updates.adoc[Auto-Updates]
** xref:rpm-ostree.adoc[bootc and rpm-ostree]
** xref:disconnected-updates.adoc[Disconnected/offline updates]
** xref:bootloader-updates.adoc[Bootloader Updates]
* Troubleshooting
** xref:manual-rollbacks.adoc[Manual Rollbacks]
** xref:access-recovery.adoc[Access Recovery]
** xref:debugging-toolbx.adoc[Using toolbx]
* Related projects
** xref:linux-desktops.adoc[Linux desktops]
** xref:fedora-coreos.adoc[Fedora CoreOS]
* Projects documentation
** https://github.com/containers/bootc[bootc]
