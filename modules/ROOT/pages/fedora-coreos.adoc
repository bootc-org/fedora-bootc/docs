= {projname} relationship with Fedora CoreOS

By far the biggest difference between the
two is that {projname} emphasizes users *deriving*
from our container base image, making arbitrary,
potentially large changes. Up to and including
custom images.

Fedora CoreOS (much like Fedora Atomic Desktops)
generally emphasizes users *configuring* an
image pre-built by Fedora, running workloads
as containers in general, but also supporting
changing config files and kernel arguments.

A good example of the division line is
https://docs.fedoraproject.org/en-US/fedora-coreos/os-extensions/[Fedora CoreOS system extensions]
aka "package layering".

If you find the need to make such changes
to the host, then you may find {projname}
a better target.

== Common components

- Fedora derivatives
- systemd, containers, ostree

== Differences

[cols="1,1"]
|===
| Fedora/CentOS bootc | Fedora CoreOS
| Datacenter/server/edge (but can be extended)
| Datacenter/server/edge 
| bootc
| ignition, rpm-ostree
| Includes python
| No python
| No cloud agents    
| afterburn            
| Anaconda, bootc-image-builder, bootc install 
| Pre-built disk images, coreos-installer
| podman
| podman, docker       
| user: root
| user: core
| composefs
| legacy ostree
| Versioned with Fedora base
| https://docs.fedoraproject.org/en-US/fedora-coreos/stream-metadata/[Custom streams]
| Automatic updates, but for *your* image
| Automatic updates from Fedora
| Targets Fedora and CentOS
| Targets Fedora only
|===
