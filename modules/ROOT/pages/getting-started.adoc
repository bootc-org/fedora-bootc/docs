= Getting Started with Bootable Containers

video::bf1xqjLeA9M[youtube,width=640,height=480,leveloffset=+4]

== What is a Bootable Container?

include::what-is-a-bootable-container.adoc[leveloffset=+1]

== Why Bootable Containers?
Before going into any further details, you may wonder about the benefits of bootable containers and why should pay attention to this technology?  Let's go through some of the main benefits.

=== A Unified Approach for DevOps
Linux already sits at the core of containers. Bootable containers take Linux’s role a step further, letting you manage the entire OS through container-based tooling and concepts, including https://www.redhat.com/en/topics/devops/what-is-gitops[GitOps] and continuous integration/continuous delivery (CI/CD). This streamlined approach helps address the challenges of managing Linux at scale, whether you're pushing patches to different locations or bridging gaps between the operations team and the application development cycle.

=== Simplified Security
While there are dozens of security benefits of image-based systems, we want to highlight the fact that the OS is shipped in the form of container images.  That means we can make use of the advancements of the past decade in container security such as advanced container security tools to address patching, scanning, validation and signing.  We can now apply container security scans on the kernel, drivers, the bootloader and more.

=== Speed and Ecosystem Integration
Similar to the security benefits, bootable containers integrate into a vast ecosystem of tools and technologies which have emerged around containers.  With bootable containers, we can build, deploy and manage our Linux systems at new scale and speed.  A consistent feedback from early adopters of bootable containers is the observation of a simplified toolchain for managing all these tasks in less time.

== Bootc
https://containers.github.io/bootc/[Bootc] is at the core and center of bootable containers. It is a CLI tool that ships with a number of systemd services and timers to manage a bootable container. Among other things, bootc is responsible for downloading and queuing updates, and can be used by other higher-level tools to manage the system and inspect the system status. For that reason, bootc is an integral part of each bootable container image. For more details, please refer to the https://containers.github.io/bootc/[bootc documentation].

== Filesystem Layout
Bootc systems follow the concept of an immutable operating system. Apart from the following two exceptions,  `/etc` and `/var`, all directories are mounted read-only once deployed on a physical or virtual machine. However, during a container build the entire file-system is writable.

The fact that most parts of the file system are mounted read-only is an important attribute of deployed bootable containers and something to consider carefully when preparing workloads and updates. For more information, see the xref:filesystem.adoc[Filesystem Layout page] which elaborate in great detail on the exact behavior.

== Base Images
At the moment, there are three distributions shipping bootable containers:

- https://fedoraproject.org/[Fedora]
- https://www.centos.org/[CentOS Stream]
- https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux[Red Hat Enterprise Linux (RHEL)]

The base images of https://fedoraproject.org[Fedora] and https://www.centos.org/centos-stream/[CentOS Stream] are listed and continuously updated on the xref:base-images.adoc[base images page]. The RHEL bootc images can be found in the https://catalog.redhat.com/software/containers/explore[Red Hat Ecosystem Catalog]; working with these requires a Red Hat account. You can get a no-cost subscription by https://developers.redhat.com/register[joining the Red Hat Developer program] in just a few clicks. You further need to login to the https://catalog.redhat.com/[Red Hat Container Registry] and register your machine with subscription-manager which is well explained in the https://www.redhat.com/en/blog/image-mode-red-hat-enterprise-linux-quick-start-guide[release blog post]. If you are using Podman Desktop, you might install the https://developers.redhat.com/articles/2024/05/07/podman-desktop-red-hat-developer-subscription[Red Hat Account Extension] which automates most of the process.

== Building bootc Images
As mentioned above, bootable containers can be built with existing tooling such as https://github.com/containers/common/blob/main/docs/Containerfile.5.md[Containerfiles] and Podman. That means you can use any existing bootc base image and customize it to your needs (e.g., install further packages, copy files from the host, run config scripts, etc.) in a container build as exemplified in the following Containerfile:

[source,subs="attributes"]
----
FROM {container-fedora-full}
RUN dnf install -y [system agents] [dependencies] && dnf clean all
COPY [unpackaged application]
COPY [configuration files]
RUN [config scripts]
----

In case the bootable container will be run in `podman-bootc`, see the xref:#_podman_bootc[podman-bootc] section for setting up `podman-machine` and building the image.

For more details on building derived bootc images, see the xref:building-containers.adoc[page in the docs]. Note that bootc is still under development. On rare occasions you might encounter problems you can find in the https://gitlab.com/fedora/bootc/tracker[upstream issue tracker] (requires a GitLab account).

== Conversion to Disk Images
Updates for bootable containers happen in the form of pulling a new image and (re-)booting into it. But how do we install a fresh bootc system? While bootc supports https://containers.github.io/bootc/bootc-install.html#using-bootc-install-to-existing-root[installing a bootc container on top of an existing system], it is more common to convert a bootable container into a so-called disk image, such as ISO, raw or qcow2 to provision a new system.

You can convert a bootable container into a disk image with the https://github.com/osbuild/bootc-image-builder[bootc-image-builder] which itself needs to be executed inside a container. `bootc-image-builder` is a feature-rich tool that further enables you to inject users, SSH keys, and define a partition layout. For more details, see the https://github.com/osbuild/bootc-image-builder?tab=readme-ov-file#-arguments[upstream documentation]. Podman Desktop also ships with a https://github.com/containers/podman-desktop-extension-bootc[bootc extension] which allows you to build and convert bootc images in just a few clicks. For more information on the extension, please see the https://developers.redhat.com/articles/2024/05/07/podman-desktop-red-hat-developer-subscription#bootc_extension_for_podman_desktop[release blog post]. Once converted, you can boot a disk image by using libvirt and qemu and other virtualization tools as described in xref:qemu-and-libvirt.adoc[in the docs].

If you are using https://podman-desktop.io[Podman Desktop], you may install the https://github.com/containers/podman-desktop-extension-bootc?tab=readme-ov-file#example-images[Podman Desktop bootc extension] to manage bootc images and automatically convert them to disk images.

== Running as a container

You may of course run the container you've built as a container, but there are important
considerations; more on this in xref:provisioning-container.adoc[Running as a container].

== podman-bootc
https://github.com/containers/podman-bootc[podman-bootc] enables a more bootc-native experience. It is a CLI tool that allows you to easily run a local bootc image in a VM and get shell access to it. Under the hood, podman-bootc uses `bootc install to-disk` to make a disk image from the container image.

podman-bootc requires the `podman-machine` package, which provides a podman machine with a rootful connection. The bootable container has to be built inside the running machine.

NOTE: The package `podman-machine` is not required on MacOS and the locally built container can be used with `podman-bootc` as it is.

The commands to run the Fedora bootc image via podman-bootc may look as follows:

[source]
----
# Initialize a new machine with root being the default user
$ podman machine init --rootful --now
----

or start an already initialized rootful podman machine:

[source]
----
$ podman machine start
----

You can list available podman machine connections to get the connection name after starting a podman machine by:

----
$ podman system connection list
Name                         URI                                                          Identity                                                   Default     ReadWrite
podman-machine-default       ssh://core@127.0.0.1:38717/run/user/1000/podman/podman.sock  /home/user/.local/share/containers/podman/machine/machine  false       true
podman-machine-default-root  ssh://root@127.0.0.1:38717/run/podman/podman.sock            /home/user/.local/share/containers/podman/machine/machine  true        true
----

Use connection `podman-machine-default-root` as a connection for `podman build` to build the image:

----
$ podman -c podman-machine-default-root build -t <repository>/<tag> <Containerfile-dir>
----

The path `<Containerfile-dir>` is a path to the directory with only `Containerfile` and files needed for build, `<repository>/<tag>` defines how the image will be named images list.

In the end run the new machine based on the Fedora base image and specify the desired filesystem:

[source]
----
$ podman-bootc run --filesystem=xfs <repository>:<tag>
----

where `<repository>:<tag>` are the values set to the image during build, and they are visible when listing available images, e.g.:

----
$ podman -c podman-machine-default-root images
REPOSITORY                   TAG         IMAGE ID      CREATED         SIZE
quay.io/user/bootc        cups        6abca60ceab2  46 minutes ago  2.05 GB
quay.io/fedora/fedora-bootc  41          7b03571fae24  3 hours ago     1.66 GB
----

podman-bootc is still under development but perfectly capable of supporting your development flow. For installation instructions and further details, please refer to the https://github.com/containers/podman-bootc[bootc upstream docs] and xref:podman-bootc-cli.adoc[section in the Fedora docs]. If you are running on a Mac, please run `brew install --head podman-bootc` to build and install the latest version.

== Authentication, Users, Groups
There are no default interactive users other than root in the base image. In the default full base images, OpenSSH is running, but there are no hardcoded credentials (passwords or SSH keys). That means that you cannot log into a booted VM without further work (e.g., injecting SSH keys for the root users). For that reason, podman-bootc automatically injects SSH keys such that you can easily get access to the VM. See the xref:authentication.adoc[section on authentication] in the docs for more information on this topic.

== Installing on Bare Metal
At this point, you know how to build a bootable container image and how to convert it into a disk image that you may want to install on your machine. Before installing Fedora/CentOS bootc, it’s recommended that you have created a customized derived container image, but this is not a hard requirement, as it is possible to enable basic system access via e.g. injecting SSH keys with kickstart or with https://containers.github.io/bootc/bootc-install.html[bootc-install] and the `--root-ssh-authorized-keys` flag. You can find a list of examples and detailed instructions xref:bare-metal.adoc[here]. You can further find documentation on how to provision public cloud instances on xref:provisioning-aws.adoc[AWS] or xref:provisioning-gcp.adoc[GCP] and how to install on xref:provisioning-vsphere.adoc[vSphere].

== Updating Bootable Containers
The figure below depicts the life cycle of a bootable container and the different steps required from building to deploying to updating bootc systems. Once a bootable container image has been built, you may convert it to a disk image. The disk image can then be used to install the content in the target environment (e.g., a public cloud instance). You might also want to push the container image to your target container registry.

image::updates.png[]

To update existing systems, the process can be repeated: that is building a new image and pushing it to the registry. Once pushed, bootc on the deployed systems can pull down the new image from the registry and reboot into the new image.

There are several ways how an individual system can be updated:

- By default, bootc systems perform **time-based updates** via a systemd timer.
- For **event-based updates**, the `bootc-fetch-apply-updates.service` can be triggered.
- **Manual updates** can be performed by running `bootc-upgrade` and rebooting the system.

Bootc further supports rollbacks via the bootc-rollback command. For more details, please refer to the xref:auto-updates[auto-updates section] which further elaborates on disconnected and bootloader updates.

The following hands-on demo shows the update process of a local VM:

video::fccox6sGCWA[youtube,width=640,height=480]


== Next Steps
At that point, we have learned the nature of and the concepts behind bootable containers. From building on top of available bootc base images to testing them locally with podman-bootc to understanding how to update already deployed systems.

The suggested next steps are to try out the technology and browse through the other pages of this documentation.


== Getting in touch

Bugs can be reported to:

- Fedora: <https://gitlab.com/fedora/bootc/base-images>
- CentOS: <https://gitlab.com/redhat/centos-stream/containers/bootc>

You can find links to further community channels on the xref:community.adoc[community] page.
