= Auto-Updates and Manual Rollbacks

== bootc

{projname} provides atomic updates and rollbacks via https://github.com/containers/bootc[bootc] deployments
for the host system.

By default, the OS performs continual auto-updates via a stock copy of the upstream
`bootc-fetch-apply-updates.timer` and corresponding `bootc-fetch-apply-updates.service`.

For more, see <https://containers.github.io/bootc/man-md/bootc-fetch-apply-updates.service.html>

== podman

Additionally for referenced application containers, the
{podman-docs}/podman-auto-update.1.html[podman-auto-update.timer]
unit can be enabled to automatically upgrade workload container images that
are explicitly configured to opt-in to automatic updates. The containers can
also be rolled back when properly configured. Please refer to the following
https://www.redhat.com/sysadmin/podman-auto-updates-rollbacks[article] for details.

include::manual-rollbacks.adoc[leveloffset=1]
