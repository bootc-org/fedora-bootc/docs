= Understanding bootc and rpm-ostree

== bootc

The main focus of {projname} is on https://github.com/containers/bootc[bootc]. However,
the "full" base image currently includes https://github.com/coreos/rpm-ostree/[rpm-ostree]
which shares a lot of the same underlying code from the https://github.com/ostreedev/ostree-rs-ext/[ostree]
project.

=== Using rpm-ostree

It is supported to install and use rpm-ostree on a {projname} system,
because the bootc and rpm-ostree projects share significant underlying code.

==== Using in container builds

At the current time, operations such as `RUN rpm-ostree install foo` when
executed as part of a container build are not substantially different
from what happens with `RUN dnf install foo` - the two codebases
ultimately share a lot via https://github.com/rpm-software-management/libdnf[libdnf].

There's not generally a strong reason to use `rpm-ostree` inside a container
build.

==== Client side operations

===== rpm-ostree upgrade/rollback

The `rpm-ostree upgrade`, `rebase`, `deploy` verbs etc. will all gracefully
interact with a `bootc` system - at the current time they operate on shared state.

You can interchange `rpm-ostree upgrade` with `bootc upgrade`, etc; they
currently perform the same operations in general.

===== rpm-ostree install/override replace

However, any local state mutations such as package layering, removals, or
enabling e.g. `rpm-ostree initramfs --enable` will cause `bootc upgrade`
to error out.

Hence if you choose to use such features, then you will need to switch over
to interacting with rpm-ostree going forward for updates.

In the more medium term, there are plans to extend `dnf` to also support
client-side functionality similar to this:
https://gitlab.com/fedora/bootc/tracker/-/issues/4[client side layering tracker].

This would build on more generic support for https://containers.github.io/bootc/booting-local-builds.html[booting local builds]
for persistent layering.
